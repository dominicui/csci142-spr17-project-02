package mvc;

import javax.swing.JOptionPane;
import model.ComputerPlayer;
import model.Player;
import model.PokerModel;

/**
 * add action to buttons
 * 
 * @author dominic
 *
 */

public class Controller 
{
	public PokerModel myModel;
	public View myView;
	private boolean myRun = true;
	private boolean myComputerFaceUp = false;
	private Player myPlayer;
	private ComputerPlayer myComputerPlayer;
	private int myComputerPlayerWins;
	private int myPlayerWins;
	private int count=0;

	public static void main(String[] args)
    {
        new Controller();
    }

	public Controller()
	{
		myPlayer = new Player(setPlayerName());
		myModel = new PokerModel(myPlayer);
		myComputerPlayer = (ComputerPlayer) myModel.getPlayer(1);
		myModel.dealCards();
		myView = new View(this);
		
	}

	public Controller(View view)
	{
		myPlayer = new Player(setPlayerName());
		myModel = new PokerModel(myPlayer);
		myComputerPlayer = (ComputerPlayer) myModel.getPlayer(1);
		myModel.dealCards();
		myView = view;
	}
	
	//each selected card will move down a little bit
	
	public void selected(Integer index)
	{
		myModel.getPlayer(0).getHand().getCards().get(index.intValue()).toggleSelected();
		if (myModel.getPlayer(0).getHand().getCards().get(index.intValue()).isSelected() == true) 
		{
			View.myCards[index.intValue()].setLocation(
					View.myCards[index.intValue()].getLocation().x, 
					View.myCards[index.intValue()].getLocation().y + 20);
		} else
		{
			View.myCards[index.intValue()].setLocation(
					View.myCards[index.intValue()].getLocation().x, 
					View.myCards[index.intValue()].getLocation().y - 20);
		}
	}

	/*
	 * start method linked start button.
	 * run this method to start a new round game
	 * player cards face up, AI cards back up
	 * 
	 */

	public void start()
	{
		count++;
		
		//restrict button continuing click twice 
		
		if(myRun == true)
		{
			myView.myRanking.setText("Your Hand is: ");
			myView.myAIRanking.setText("AI Hand is: ");
			myView.myMessage.setText("Please select bad cards��");
			
			myModel.dealCards();
			myView.flipCards();

			//make sure first button click does not make AI cards face up
			
			if(count > 1)
			{
				myView.flipComputerCards();
			}
			
			if(myComputerFaceUp == true)
			{
				myView.flipComputerCards();
			}
			
			myRun = false;
		}
	}

	/*
	 * discard method linked discard button.
	 * run this method to change the cards that selected
	 * player and AI cards both face up
	 * 
	 */
	
	public void discard()
	{
		//restrict button continuing click twice 
		
		if(myRun == false)
		{
			myPlayer.getHand().discard();
			myModel.dealCards();
	
			myView.myRanking.setText("Your Hand is: "
			        + myModel.getPlayer(0).getHand().determineRanking().name());
			
			myView.flipCards();
			myModel.switchTurns();
			
			myView.myMessage.setText(myModel.getPlayer(1).getName()
					+ " removed "+myComputerPlayer.selectCardsToDiscard().size()
					+ " cards.");
			
			myModel.getPlayer(1).getHand().discard(myComputerPlayer.selectCardsToDiscard());
			myModel.dealCards();
			
			myView.myAIRanking.setText("AI Hand is: "
			        + myModel.getPlayer(1).getHand().determineRanking().name());
			
			helpCounting();
			
			myView.flipComputerCards();
			myView.myPlayerWins.setText("Wins: "+ myPlayerWins);
			myView.myAIWins.setText("Wins: "+ myComputerPlayerWins);
			myView.myMessenger.setText(myModel.determineWinner().getName() 
					+ " win with " 
					+ myModel.determineWinner().getHand().determineRanking().name());
			myRun = true;
			myComputerFaceUp = true;
	    	myModel.resetGame();	    	
		}		
	}

	//exit game
	
	public void exit()
	{
		System.exit(0);
	}
	
	//collect player name
	
	public static String setPlayerName()
	{
		String name = JOptionPane.showInputDialog(null,"Please enter your name?",null,JOptionPane.INFORMATION_MESSAGE);
		return name;
	}

	/*
	 * getNumberWins does not show the right win number
	 * I did not figure out why
	 * just make it to display right wins
	 * 
	 */
			
	public void helpCounting()
	{
		myModel.determineWinner();
		if(myModel.determineWinner() == myComputerPlayer)
		{
			myComputerPlayerWins ++;
		}
		else
		{
			myPlayerWins ++;
		}
	}
}