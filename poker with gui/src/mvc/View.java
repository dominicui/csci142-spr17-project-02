package mvc;

import java.awt.Font;
import java.awt.Image;
import java.lang.reflect.Method;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * graphical user interface for poker game
 * 
 * @author dominic
 *
 */

public class View 
{
	private Controller myController;
	public JFrame myBoard;
	private JPanel myPlayerPanel, 
	               myComputerPanel;
	public JLabel myPlayer, 
                  myPlayerWins, 
                  myAI, 
                  myAIWins, 
                  myMessenger, 
                  myMessage, 
                  myRanking, 
                  myAIRanking;
	private JButton myStart, 
                    myDiscard,
                    myExit;
	static JLabel[] myCards,
                    myComputerCards;
	private ButtonListener[] myCardListener;
	private ButtonListener myStartListener, 
	                       myDiscardListener,
	                       myExitListener;
	
	public View(Controller controller) 
	{
		myBoard = new JFrame();
		myBoard.setLayout(null);
		myBoard.setSize(1200, 1000);

		myController = controller;
		
		Font font = new Font("Roman", Font.ROMAN_BASELINE, 28);
		
		//set player information: name and number of wins
		
		myPlayer = new JLabel(myController.myModel.getPlayer(0).getName());
		myPlayer.setFont(font);
		myPlayer.setLocation(20, 20);
		myPlayer.setSize(200, 30);
		
		myBoard.add(myPlayer);
		myPlayerWins = new JLabel("Wins: 0");
		myPlayerWins.setFont(font);
		myPlayerWins.setLocation(20, 50);
		myPlayerWins.setSize(200, 30);
		myBoard.add(myPlayerWins);

		myAI = new JLabel(myController.myModel.getPlayer(1).getName());
		myAI.setFont(font);
		myAI.setLocation(1000, 20);
		myAI.setSize(200, 30);
		myBoard.add(myAI);

		myAIWins = new JLabel("Wins: 0");
		myAIWins.setFont(font);
		myAIWins.setLocation(1000, 50);
		myAIWins.setSize(200, 30);
		myBoard.add(myAIWins);

		//set each hand display for players and messengers to tell hand ranking
		
		myComputerPanel = new JPanel();
		myComputerPanel.setLocation(100, 100);
		myComputerPanel.setSize(1000, 300);
		
		myComputerCards = new JLabel[5];
		for (int i = 0; i < 5; i++) 
		{
			Image AIImage = myController.myModel.getPlayer(0).getHand().getCards().get(i).getImage();
			myComputerCards[i] = new JLabel(new ImageIcon(AIImage));
			myComputerPanel.add(myComputerCards[i]);
		}
		myBoard.add(myComputerPanel);

		myAIRanking = new JLabel("AI Hand is:");
		myAIRanking.setLocation(500, 50);
		myAIRanking.setSize(500, 50);
		myBoard.add(myAIRanking);

		myPlayerPanel = new JPanel();
		myPlayerPanel.setLocation(100, 450);
		myPlayerPanel.setSize(1000, 300);
		
		myCards = new JLabel[5];
		for (int i = 0; i < 5; i++) 
		{
			Image playerImage = myController.myModel.getPlayer(0).getHand().getCards().get(i).getImage();
			myCards[i] = new JLabel(new ImageIcon(playerImage));
			myCards[i].setSize(200, 300);
			myPlayerPanel.add(myCards[i]);
		}

		myBoard.add(myPlayerPanel);

		myRanking = new JLabel("Your Hand is:");
		myRanking.setLocation(500, 400);
		myRanking.setSize(500, 50);
		myBoard.add(myRanking);

		//two functional buttons to play game 
		
		myStart = new JButton("Start");
		myStart.setFont(font);
		myStart.setLocation(400, 750);
		myStart.setSize(100, 50);
		myBoard.add(myStart);

		myDiscard = new JButton("Discard");
		myDiscard.setFont(font);
		myDiscard.setLocation(500, 750);
		myDiscard.setSize(150, 50);
		myBoard.add(myDiscard);
		
		//new button added to exit game
		
		myExit = new JButton("Exit");
		myExit.setFont(font);
		myExit.setLocation(650, 750);
		myExit.setSize(100, 50);
		myBoard.add(myExit);

		//extra information section
		
		myMessenger = new JLabel("Win Messenger: ");
		myMessenger.setFont(font);
		myMessenger.setLocation(600, 800);
		myMessenger.setSize(1000, 100);
		myBoard.add(myMessenger);

		myMessage = new JLabel("Process Messenger:");
		myMessage.setFont(font);
		myMessage.setLocation(20, 800);
		myMessage.setSize(1000, 100);
		myBoard.add(myMessage);

		this.associateListeners();
		myBoard.setVisible(true);
		myBoard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Associates each component's listener with the controller
	 * 
	 */
	
	public void associateListeners() 
	{
		Class<? extends Controller> controllerClass;
		Method startMethod, discardMethod, selectedMethod, exitMethod;
		Class<?>[] classArgs;

		controllerClass = myController.getClass();

		startMethod = null;
		discardMethod = null;
		selectedMethod = null;
		exitMethod = null;
		
		classArgs = new Class[1];

		try 
		{
			classArgs[0] = Class.forName("java.lang.Integer");
		} 
		catch (ClassNotFoundException e) 
		{
			String error;
			error = e.toString();
			System.out.println(error);
		}
		try 
		{
			startMethod = controllerClass.getMethod("start", (Class<?>[]) null);
			discardMethod = controllerClass.getMethod("discard", (Class<?>[]) null);
			selectedMethod = controllerClass.getMethod("selected", classArgs);
			exitMethod = controllerClass.getMethod("exit", (Class<?>[]) null);
		} 
		catch (NoSuchMethodException exception) 
		{
			String error;

			error = exception.toString();
			System.out.println(error);
		} 
		catch (SecurityException exception) 
		{
			String error;

			error = exception.toString();
			System.out.println(error);
		}

		int i;
		Integer[] args;
		
		myStartListener = new ButtonListener(myController, startMethod, null);
		myStart.addMouseListener(myStartListener);
		myDiscardListener = new ButtonListener(myController, discardMethod, null);
		myDiscard.addMouseListener(myDiscardListener);
		myExitListener = new ButtonListener(myController, exitMethod, null);
		myExit.addMouseListener(myExitListener);

		myCardListener = new ButtonListener[5];
		
		//add button listener to every single card
		
		for (i = 0; i < 5; i++)
		{
			args = new Integer[1];
			args[0] = new Integer(i);
			myCardListener[i] = new ButtonListener(myController, selectedMethod, args);
			myCards[i].addMouseListener(myCardListener[i]);
		}
	}

	//show the face or back of player's cards
	
	public void flipCards()
	{
		for (int i = 0; i < 5; i++) 
		{
			if (myController.myModel.getPlayer(0).getHand().getCards().get(i).isFaceUp() == false) 
			{
				myController.myModel.getPlayer(0).getHand().getCards().get(i).flip();
			}
			ImageIcon icon = new ImageIcon(myController.myModel.getPlayer(0).getHand().getCards().elementAt(i).getImage());
			myCards[i].setIcon(icon);
			myPlayerPanel.add(myCards[i]);
		}
	}

	//show the face or back of AI's cards
	
	public void flipComputerCards() 
	{
		for (int i = 0; i < 5; i++) 
		{
			myController.myModel.getPlayer(1).getHand().getCards().get(i).flip();
			ImageIcon icon = new ImageIcon(myController.myModel.getPlayer(1).getHand().getCards().elementAt(i).getImage());
			myComputerCards[i].setIcon(icon);
			myComputerPanel.add(myComputerCards[i]);
		}
	}

}
